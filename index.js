//Operators

//Assignment Operators

// Basic Assignment Operator (=)
let assignmenNumber = 8;

// Arithmetic Assignment Operators
// Addition Assignment Operators (+=)


assignmenNumber = assignmenNumber + 2
assignmenNumber +=  2;
console.log("Result of Addition Assignment Operator: "+assignmenNumber);

//Subtraction Assignment Operators (-=)
assignmenNumber -= 2;
console.log("Result of Subtraction Assignment Operator: "+assignmenNumber);

//Multiplication Assignment Operators (*=)
assignmenNumber *=2;
console.log("Result of Multiplication Assignment Operator: "+assignmenNumber);

//Division Assignment Operators (/=)
assignmenNumber /=2;
console.log("Result of Division Assignment Operator: "+assignmenNumber);

//Arethmetic Operators
let x = 1397;
let y = 7831;

let sum =  x + y;
console.log("Result of Addition operator is: "+sum);

let difference =  x - y;
console.log("Result of Subtraction operator is: "+difference);

let product =  x * y;
console.log("Result of Multiplication operator is: "+product);

let quotient =  x / y;
console.log("Result of Division operator is: "+quotient);

let remainder = x % y;
console.log("Result of Modulus operator is: "+remainder);

//Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;

console.log("Result of MDAS operation is: "+mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of PEMDAS operation is: "+pemdas);

//Increment (++) and Decrement (--)

let z = 1;
let increment = ++z; //Pre-Increment
console.log("Result of pre-increment  is: "+increment);
console.log("Result of pre-increment  is: "+z);

increment = z++; //Post-Increment
console.log("Result of post-increment  is: "+increment);
console.log("Result of post-increment  is: "+ z); 

let decrement = --z; //Pre-Decrement
console.log("Result of pre-decrement  is: "+decrement);
console.log("Result of pre-decrement  is: "+z);

decrement = z--; //Post-Decrement
console.log("Result of post-decrement  is: "+decrement);
console.log("Result of post-decrement  is: "+ z); 

//Type coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB; //concatinate
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC); //true = 1, false = 0

let numD = false + 1;
console.log(numD);

//Relational Operators

//Equality Operator(==)

let juan = 'juana';
console.log(1==1);
console.log(1==2);
console.log(1=='1'); //true
console.log('juan' == juan);


//Inequality Operator(!=)

console.log(1!=1);
console.log(1!=2);
console.log(1!='1'); //false
console.log('juan' != juan);

//<, >, <=, >=

console.log(4 < 6); //true
console.log(2 > 8); //false
console.log(5 >= 5); //true
console.log(10 <= 15); //true


//Strict Equality Operator (===)

console.log(1==='1');//false

//Strict Inequality Operator (===)

console.log(1!=='1');//true

//LOGICAL OPERATORS

let isLegalAge = true;
let isRegistered = false;

//AND (&&) Operator - return TRUE if all operands are true.
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND operator: "+allRequirementsMet);


//OR (||) Operator -return if one of the operands is true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR operator: "+someRequirementsMet);

console.log("-----------------------");

//Selection Control Structures

// IF, ELSE IF, ELSE STATEMENT

//IF STATEMENT - Executes a stement if a specified condition is true.

let numE = -1;

if(numE < 0){
	console.log("Hello");
}

let nickName = 'Mattheo';

if(nickName ==='Matt'){
	console.log("hello "+nickName)
}

//ELSE IF CLAUSE - executes a statement if previous conditions are false and if the specified condition is true.

let numF = 1;
if(numE > 0){
	console.log("Hello");
}
else if(numF > 0){
	console.log("World");
}

//ELSE CLAUSE - executes a statement if all conditions are not met.


if(numE > 0){
	console.log("Hello");
}
else if(numF === 0){
	console.log("World");
}
else{
	console.log("Again");
}

//TERNARY OPERATOR

/* SYNTAX:
	(expression) ? ifTrue : ifFalse;

*/

let ternaryResult = (1<18) ? true : false;
console.log("Result of Ternary Operator: "+ternaryResult)

/*

let name;

function isOfLegalAge(){
	name='John'
	return 'you are of the legal age limit'

}


function isUnderAge(){
	name= 'Jane'
	return 'you are under the age limit'
}

let age = parseInt(prompt("What is your age? "));
console.log(age)

let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
console.log("result of ternary operator in function: " + legalAge + " " + name);

*/

//SWITCH Statement

/*SYNTAX


switch(expression){
	case value1:
		statement;
		break;

	case value2:
		statement;
		break;

	case valueN:
		statement;
		break;

	default:
		statement;

}

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case 'sunday':
		console.log("the color of the day is red");
		break;


	case 'sunday':
		console.log("the color of the day is red");
		break;

	case 'monday':
		console.log("the color of the day is orange");
		break;

	case 'tuesday':
		console.log("the color of the day is yellow");
		break;

	case 'wednesday':
		console.log("the color of the day is green");
		break;

	case 'thursday':
		console.log("the color of the day is blue");
		break;

	case 'friday':
		console.log("the color of the day is indigo");
		break;

	case 'saturday':
		console.log("the color of the day is violet");
		break;

	default:
		console.log("Please input a valid day")

}

//Try-Catch-Finally Statement


/*

function showIntensityAlert(windSpeed){
	try{
		//Attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed));
	}

	catch(err){
		conole.log(typeof err);

		console.warn(err.message);

		//err.message is used to access the information relating to an error object
	}

	finally{
		//continue on executing the code regardless of success or failure of code execution in the 'try' block
		alert('Intensity updates will show new alert.');
	}
	
}


*/




